MQ demo
=======

demo: http://64.225.107.162:5005

Architecture
------------

### Services

    +---------+
    |interface|
    +--+---+--+
       |   ^
       |   |
       v   |
    +--+---+--+
    |rabbitmq |
    +--+---+--+
       |   ^
       |   |
       v   |
    +--+---+--+
    | storage |
    +---------+


### MQ communication


    interface                                 storage
       +             set_val                     +
       + --------------------------------------> +
       |                                         |
       |                                         |
       |                                         |
       |             get_val                     |
       + --------------------------------------> +
       |             get_val_resp                |
       + <-------------------------------------- +
       |                                         |
       |                                         |
       +                                         +


Api docs
--------

http://0.0.0.0:5005/docs


### Build and run


Build:

```bash
make build
# or
make rebuild
```


Run:

```bash
make run
# or
make run_in_background
```


Known issues
------------

- Hardcoded RabbitMQ connection string
- _Probably_ using different queues for different types of messages isn't right approach. Set headers instead?
- Messages body format are in general crap
- Api returns empty string as val instead 404 on key not found


FAQ
---

### `touch: /etc/rabbitmq/rabbitmq.conf: Permission denied`

Error:

```
rabbitmq     | touch: /etc/rabbitmq/rabbitmq.conf: Permission denied
rabbitmq exited with code 1
```

Fix: 

```bash
echo 'yolo, lol'; sudo chmod 777 .docker -R
```
