import asyncio

from app.consumers import (
    handle_kv_get,
    handle_kv_set,
)


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    asyncio.ensure_future(handle_kv_get(loop))
    asyncio.ensure_future(handle_kv_set(loop))
    loop.run_forever()
