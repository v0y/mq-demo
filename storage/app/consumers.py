import aio_pika

from app.handlers import (
    kv_get,
    kv_set,
)


# FIXME: lots of duplicated code


async def handle_kv_get(loop) -> None:
    connection = await aio_pika.connect_robust(
        'amqp://user:pass@rabbitmq:5672/',
        loop=loop,
    )

    async with connection:
        channel = await connection.channel()

        queue = await channel.declare_queue('kv_get', auto_delete=True)
        async with queue.iterator() as queue_iter:
            async for message in queue_iter:
                async with message.process():
                    msg = message.body.decode('utf-8')
                    print(f'received kv_get:{msg}')
                    await kv_get(msg)


async def handle_kv_set(loop) -> None:
    connection = await aio_pika.connect_robust(
        'amqp://user:pass@rabbitmq:5672/',
        loop=loop,
    )

    async with connection:
        channel = await connection.channel()

        queue = await channel.declare_queue('kv_set', auto_delete=True)
        async with queue.iterator() as queue_iter:
            async for message in queue_iter:
                async with message.process():
                    msg = message.body.decode('utf-8')
                    print(f'received kv_set:{msg}')
                    await kv_set(msg)
