import asyncio

import aio_pika


async def publish_resp(val):
    loop = asyncio.get_event_loop()

    connection = await aio_pika.connect(
        # FIXME: hardcoded credentials
        'amqp://user:pass@rabbitmq:5672/',
        loop=loop
    )

    async with connection:
        routing_key = 'kv_get_resp'

        channel = await connection.channel()
        body = bytes(str(val), 'UTF-8')

        print(f'publishing {routing_key}:{body}')

        await channel.default_exchange.publish(
            aio_pika.Message(body=body),
            routing_key=routing_key,
        )
