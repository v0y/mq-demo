import json
from ast import literal_eval
from contextlib import suppress
from pathlib import Path

from app.publishers import publish_resp


db_path = Path('.') / 'db' / 'db.json'


async def kv_get(msg: str) -> None:
    try:
        with open(db_path, 'r') as db:
            db_json = json.load(db)
    except FileNotFoundError:
        await publish_resp('')
    else:
        await publish_resp(db_json.get(msg, ''))


async def kv_set(msg: str) -> None:
    data = literal_eval(msg)
    db_json = {}

    # load db
    with suppress(FileNotFoundError):
        with open(db_path, 'r') as db:
            db_json = json.load(db)

    # write db
    db_json[data['key']] = data['val']
    with open(db_path, 'w') as db:
        json.dump(db_json, db)
