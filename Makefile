.PHONY: run build rebuild run_in_background

TEST_COMMAND=LEAD_BOOKSY_ACCOUNT_PASSWORDS_KEY=nNYIRKPKpl5jn4y_8EozTu-IxvXqMQnF7yRRxk1rlmU=\
 py.test -v

build:
	docker-compose build

run:
	docker-compose up

run_in_background:
	docker-compose up -d

rebuild: build run_in_background
