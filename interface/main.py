import asyncio
import logging

from fastapi import FastAPI
from hypercorn.asyncio import serve
from hypercorn.config import Config

from app.api.endpoints import router


log = logging.getLogger('__name__')


app = FastAPI()
app.include_router(router)


if __name__ == '__main__':
    config = Config.from_mapping(
        bind='0.0.0.0:5005',
        loglevel='DEBUG',
        use_reloader=True,
    )

    loop = asyncio.get_event_loop()
    loop.create_task(serve(app, config))
    loop.run_forever()

