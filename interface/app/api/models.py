from pydantic import BaseModel


class KeyVal(BaseModel):
    key: str
    val: str
