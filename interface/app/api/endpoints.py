from fastapi import APIRouter
from starlette.responses import RedirectResponse

from app.api.models import KeyVal
from app.mq import (
    get_val,
    set_val,
)


router = APIRouter()


@router.get('/')
async def index():
    return RedirectResponse('/docs')


@router.get('/kv/{key}', response_model=KeyVal)
async def get_val_api(key: str):
    val = await get_val(key)
    return {'key': key, 'val': val}


@router.post('/kv/', response_model=KeyVal)
async def set_val_api(data: KeyVal):
    await set_val(**data.dict())
    return data
