import asyncio

import aio_pika


async def set_val(**kwargs):
    loop = asyncio.get_event_loop()

    connection = await aio_pika.connect(
        # FIXME: hardcoded credentials
        'amqp://user:pass@rabbitmq:5672/',
        loop=loop
    )

    async with connection:
        routing_key = 'kv_set'

        channel = await connection.channel()
        body = bytes(str(kwargs), 'UTF-8')

        print(f'publishing {body}')

        await channel.default_exchange.publish(
            aio_pika.Message(body=body),
            routing_key=routing_key,
        )

async def get_val(key):
    loop = asyncio.get_event_loop()

    connection = await aio_pika.connect_robust(
        'amqp://user:pass@rabbitmq:5672/',
        loop=loop,
    )

    async with connection:
        channel = await connection.channel()

        # send req
        body = bytes(key, 'UTF-8')

        print(f'publishing kv_get:{body}')

        await channel.default_exchange.publish(
            aio_pika.Message(body=body),
            routing_key='kv_get',
        )

        # get resp
        queue = await channel.declare_queue('kv_get_resp', auto_delete=True)

        async with queue.iterator() as queue_iter:
            async for message in queue_iter:
                async with message.process():
                    val = message.body.decode('utf-8')

                    print(f'received kv_get_resp:{val}')

                    return val
